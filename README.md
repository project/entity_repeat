# ENTITY REPEAT

This module allows you to repeat entities based on repeating criteria.
Only entities that contain date fields can be repeated. The repeating
process will automatically generate copies of the entity, changing the
date to represent the repeating pattern.

## Dependencies

- [Replicate](https://www.drupal.org/project/replicate)
- [Recurring Dates Field](https://www.drupal.org/project/date_recur)
- [Recurring Date Field Modular Widgets](https://www.drupal.org/project/date_recur_modular)

## Installation and setup

1.  Install the module as you would any other.
2.  Add a Recurring Dates field to an entity bundle and set the field
    widget to Entity Repeat widget.
3.  Toggle permissions under Entity Repeat to your liking.
4.  A checkbox allowing for entity generation will now display along
    with the Recurring Dates field if the user has permission.

## Repeating options

See the [Recurring Dates Field](https://www.drupal.org/project/date_recur) module for all of the available options.
