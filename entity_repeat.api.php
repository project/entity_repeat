<?php

/**
 * @file
 * Hooks provided by Entity Repeat module.
 */

/**
 * Allow modules to alter the entity generation.
 *
 * @param object $entity
 *   The entity being repeated.
 * @param object $helper
 *   The date_recur helper.
 * @param array $dates
 *   The dates to be recurred.
 */
function hook_entity_repeat_generate_alter($entity, $helper, array $dates) {

}

/**
 * Allow modules to alter the entity creation.
 *
 * @param object $clone
 *   The entity being created.
 * @param object $recur_field
 *   The entity's recur-enabled date field.
 * @param array $context
 *   The start and end dates.
 */
function hook_entity_repeat_create_entity_alter($clone, $recur_field, array $context) {

}
