<?php

namespace Drupal\entity_repeat;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides dynamic permissions for nodes of different types.
 */
class EntityRepeatPermissions {
  use BundlePermissionHandlerTrait;
  use StringTranslationTrait;

  /**
   * Returns an array of permissions.
   *
   * @return array
   *   The permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function permissions() {
    // Get entity bundles with Recurring enabled on a date field.
    $bundles = _entity_repeat_get_bundles();

    return $this->generatePermissions($bundles, [$this, 'buildPermissions']);
  }

  /**
   * Returns a list of entity permissions for a given entity type.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBundleBase $bundle_type
   *   The bundle that the permissions apply to.
   *
   * @return array[]
   *   An array of permission permissions.
   */
  protected function buildPermissions(ConfigEntityBundleBase $bundle_type) {
    $entity_type_id = $bundle_type->getEntityType()->getBundleOf();
    $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    $bundle_id = $bundle_type->id();

    $params = [
      '%type_name' => $entity_type->getLabel(),
      '%bundle_name' => $bundle_type->label(),
    ];
    return [
      "repeat own $bundle_id $entity_type_id" => [
        'title' => $this->t('%type_name: Repeat own %bundle_name', $params),
      ],
      "repeat any $bundle_id $entity_type_id" => [
        'title' => $this->t('%type_name: Repeat any %bundle_name', $params),
      ],
    ];
  }

}
