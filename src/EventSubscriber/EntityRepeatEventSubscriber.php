<?php

namespace Drupal\entity_repeat\EventSubscriber;

use Drupal\date_recur\Event\DateRecurEvents;
use Drupal\date_recur\Event\DateRecurValueEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscribers for Recurring Date Field.
 */
class EntityRepeatEventSubscriber implements EventSubscriberInterface {

  /**
   * Dispatched after an entity containing a date recur field is saved.
   *
   * @param \Drupal\date_recur\Event\DateRecurValueEvent $event
   *   The date recur value event.
   *
   * @see \Drupal\date_recur\Event\DateRecurEvents::FIELD_VALUE_SAVE
   *   Event name.
   * @see \Drupal\date_recur\Plugin\Field\FieldType\DateRecurFieldItemList::postSave
   *   Dispatched by.
   */
  public function onSave(DateRecurValueEvent $event) {
    $field = $event->getField();

    // Get the entity the recur field is on.
    $entity = $field->getEntity();

    // Only generate entities if recurring has been set.
    if (empty($entity->data['entity_repeat_enabled'])) {
      return;
    }

    // Un-set to avoid infinite loop.
    unset($entity->data['entity_repeat_enabled']);

    $helper = $field->get(0)->getHelper();

    // Get date occurrences.
    $dates = $helper->getOccurrences();

    // Remove first date so we don't duplicate the original entity (same date).
    array_shift($dates);

    // Generate the entities.
    _entity_repeat_generate_entities($entity, $helper, $dates);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DateRecurEvents::FIELD_VALUE_SAVE => ['onSave'],
    ];
  }

}
