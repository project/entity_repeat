<?php

namespace Drupal\entity_repeat\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_recur_modular\Plugin\Field\FieldWidget\DateRecurModularAlphaWidget as parentAlias;

/**
 * Entity Recur widget.
 *
 * @FieldWidget(
 *   id = "entity_repeat",
 *   label = @Translation("Entity Repeat widget"),
 *   field_types = {
 *     "date_recur",
 *   }
 * )
 */
class EntityRepeatWidget extends parentAlias {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $entity = $form_state->getformObject()->getEntity();

    // Check user permission to recur.
    if (!$this->entityRepeatAccess($entity)) {
      return $element;
    }

    // Remove infinite repeat option as it is not viable.
    unset($element['ends_mode']['#options']['infinite']);
    // Unset default option.
    unset($element['ends_mode']['#default_value']);
    // Make date the first option.
    $element['ends_mode']['#options'] = ['date' => $element['ends_mode']['#options']['date']] + $element['ends_mode']['#options'];

    $element['entity_repeat_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replicate this entity for each of the generated dates'),
    ];

    $element['#theme'] = 'entity_repeat_widget';

    return $element;
  }

  /**
   * Checks access to repeat the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool
   */
  public function entityRepeatAccess(EntityInterface $entity) {
    $access = FALSE;
    $account = \Drupal::currentUser();
    $author = $entity->getOwner();
    $bundle_id = $entity->bundle();
    $entity_type_id = $entity->getEntityTypeId();

    // User can repeat any content of this type.
    if ($account->hasPermission("repeat any $bundle_id $entity_type_id")) {
      $access = TRUE;
    }

    // User is the author and can repeat their own content.
    if ($account->id() == $author->id() && $account->hasPermission("repeat own $bundle_id $entity_type_id")) {
      $access = TRUE;
    }

    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateModularWidget(array &$element, FormStateInterface $form_state, array &$complete_form): void {
    parent::validateModularWidget($element, $form_state, $complete_form);

    $fieldname = $element['#parents'][0];
    $entity_repeat_enabled = $form_state->getValue($fieldname)[0]['entity_repeat_enabled'];
    if (!$entity_repeat_enabled) {
      return;
    }

    $entity = $form_state->getformObject()->getEntity();
    $entity->data['entity_repeat_enabled'] = 1;

    $start = $form_state->getValue(array_merge($element['#parents'], ['start']));
    $end = $form_state->getValue(array_merge($element['#parents'], ['end']));
    $mode = $form_state->getValue(array_merge($element['#parents'], ['mode']));
    $ends_mode = $form_state->getValue(array_merge($element['#parents'], ['ends_mode']));
    $ends_date = $form_state->getValue(array_merge($element['#parents'], ['ends_date']));
    $ends_count = $form_state->getValue(array_merge($element['#parents'], ['ends_count']));

    // Start and end dates must be provided.
    if (!$start instanceof DrupalDateTime) {
      $form_state->setError($element['start'], t('Start date must be provided.'));
    }
    if (!$end instanceof DrupalDateTime) {
      $form_state->setError($element['end'], t('End date must be provided.'));
    }

    // A limit to repeats must be provided if repeating more than once and not a specific number of days (multiday).
    if ($mode !== 'once' && $mode !== 'multiday') {
      // An end mode must be provided.
      if (empty($ends_mode)) {
        $form_state->setError($element['ends_mode'], t('An end date or limit on occurrences must be provided.'));
      }
      // Date must be provided if ends mode is date.
      if ($ends_mode == 'date' && !$ends_date instanceof DrupalDateTime) {
        $form_state->setError($element['ends_date'], t('Date to end repeats must be provided.'));
      }
      // Count must be provided if ends mode is count.
      if ($ends_mode == 'count' && empty($ends_count)) {
        $form_state->setError($element['ends_count'], t('Number of occurrences must be provided.'));
      }
    }

  }

}
