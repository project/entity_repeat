<?php

namespace Drupal\Tests\entity_repeat\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Master class for Entity Recur tests.
 */
abstract class EntityRepeatTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->admin_user = $this->drupalCreateUser([
      'bypass node access',
      'repeat any page content',
    ]);
    $this->regular_user = $this->drupalCreateUser();
    $this->drupalLogin($this->admin_user);
  }

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['entity_repeat'];

}
